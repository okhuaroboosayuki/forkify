import * as model from "./model.js";
import recipeView from "./view/recipeView.js";
import searchView from "./view/searchView.js";
import searchResultsView from "./view/searchResultsView.js";
import paginationView from "./view/paginationView.js";
import bookmarkView from "./view/bookmarkView.js";
import addRecipeView from "./view/addRecipeView.js";
import { MODAL_CLOSE_TIMEOUT_SECONDS } from "./config.js";

import "core-js/stable";
import "regenerator-runtime/runtime";

// preserves state
if (module.hot) {
  module.hot.accept();
}

const showRecipe = async function () {
  try {
    const id = window.location.hash.slice(1);

    if (!id) return;

    recipeView.renderSpinner();

    // update results to mark selected search result
    searchResultsView.update(model.getSearchResultsPage());
    bookmarkView.update(model.state.bookmarks);

    // loading recipe
    await model.loadRecipe(id);

    // rendering recipe
    recipeView.render(model.state.recipe);
  } catch (error) {
    recipeView.renderError();
  }
};

const controlSearch = async function () {
  try {
    searchResultsView.renderSpinner();

    // get search query
    const query = searchView.getQuery();
    if (!query) return;

    // load search results
    await model.loadSearch(query);

    // render search results
    searchResultsView.render(model.getSearchResultsPage());

    // render pagination
    paginationView.render(model.state.search);
  } catch (error) {
    console.error(error);
  }
};

const controlPagination = function (goToPage) {
  // render new search results
  searchResultsView.render(model.getSearchResultsPage(goToPage));

  // render new pagination btns
  paginationView.render(model.state.search);
};

const controlServings = function (newServings) {
  // update recipe serving state
  model.updateServings(newServings);

  // update recipe view
  recipeView.update(model.state.recipe);
};

const controlAddBookmark = function () {
  if (!model.state.recipe.bookmarked) model.addBookmark(model.state.recipe);
  else model.deleteBookmark(model.state.recipe.id);

  // update DOM
  recipeView.update(model.state.recipe);

  // render bookmarks
  bookmarkView.render(model.state.bookmarks);
};

const controlBookmarks = function () {
  bookmarkView.render(model.state.bookmarks);
};

const controlAddRecipe = async function (newRecipe) {
  try {
    // spinner
    addRecipeView.renderSpinner();

    // upload new recipe
    await model.uploadRecipe(newRecipe);

    // render recipe
    recipeView.render(model.state.recipe);

    // display success message
    addRecipeView.renderSuccessMessage();

    // render bookmark view
    bookmarkView.render(model.state.bookmarks);

    // change url id
    window.history.pushState(null, "", `#${model.state.recipe.id}`);

    // close form window
    setTimeout(function () {
      addRecipeView.toggleWindow();
    }, MODAL_CLOSE_TIMEOUT_SECONDS * 1000);
  } catch (error) {
    console.error(error);
    addRecipeView.renderError(error.message);
  }
};

const init = function () {
  bookmarkView.addRenderHandler(controlBookmarks);
  recipeView.addRenderHandler(showRecipe);
  recipeView.addUpdateServingsHandler(controlServings);
  recipeView.addBookmarkHandler(controlAddBookmark);
  searchView.addSearchHandler(controlSearch);
  paginationView.addPaginationBtnHandler(controlPagination);
  addRecipeView.addUploadHandler(controlAddRecipe);
};

init();
