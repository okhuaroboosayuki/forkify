export const API_URL = `https://forkify-api.herokuapp.com/api/v2/recipes/`;
export const TIMEOUT_SECONDS = 10;
export const RESULTS_PER_PAGE = 10;
export const API_KEY = "b0bc5191-e2e7-4cba-8516-875b9f9c410f";
export const MODAL_CLOSE_TIMEOUT_SECONDS = 2.5;
