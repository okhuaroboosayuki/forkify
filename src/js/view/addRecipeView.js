import icons from "url:../../img/icons.svg"; // gets url for icons

import View from "./View";

class AddRecipeView extends View {
  _successMessage = "Your recipe was successfully uploaded";

  _parentElement = document.querySelector(".upload");
  _window = document.querySelector(".add-recipe-window");
  _overlay = document.querySelector(".overlay");
  _openBtn = document.querySelector(".nav__btn--add-recipe");
  _closeBtn = document.querySelector(".btn--close-modal");

  constructor() {
    super();
    this._addShowWindowHandler();
    this._addHideWindowHandler();
  }

  toggleWindow() {
    this._overlay.classList.toggle("hidden");
    this._window.classList.toggle("hidden");
  }

  _addShowWindowHandler() {
    this._openBtn.addEventListener("click", this.toggleWindow.bind(this));
  }

  _addHideWindowHandler() {
    this._closeBtn.addEventListener("click", this.toggleWindow.bind(this));
    this._overlay.addEventListener("click", this.toggleWindow.bind(this));
  }

  addUploadHandler(handlerFn) {
    this._parentElement.addEventListener("submit", function (e) {
      e.preventDefault();

      const dataArray = [...new FormData(this)];
      const data = Object.fromEntries(dataArray);

      handlerFn(data);
    });
  }
}

export default new AddRecipeView();
