import View from "./View";
import recipePreview from "./recipePreview";

class SearchResultsView extends View {
  _parentElement = document.querySelector(".results");
  _errorMessage = "No recipes found for your search! Try another recipe";
  _successMessage;

  _generateMarkup() {
    return this._data
      .map((searchResult) => recipePreview.render(searchResult, false))
      .join("");
  }
}

export default new SearchResultsView();
