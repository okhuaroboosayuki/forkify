import View from "./View";
import recipePreview from "./recipePreview";

class BookmarkView extends View {
  _parentElement = document.querySelector(".bookmarks__list");
  _errorMessage = "No bookmarks yet! Bookmark a recipe";
  _successMessage;

  addRenderHandler(handlerFn) {
    window.addEventListener("load", handlerFn);
  }

  _generateMarkup() {
    return this._data
      .map((bookmark) => recipePreview.render(bookmark, false))
      .join("");
  }
}

export default new BookmarkView();
