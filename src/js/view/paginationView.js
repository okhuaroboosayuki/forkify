import icons from "url:../../img/icons.svg"; // gets url for icons

import View from "./View";

class PaginationView extends View {
  _parentElement = document.querySelector(".pagination");

  addPaginationBtnHandler(handlerFn) {
    this._parentElement.addEventListener("click", function (e) {
      const btn = e.target.closest(".btn--inline");

      if (!btn) return;

      const goToPage = btn.dataset.goto;
      console.log(goToPage);

      handlerFn(goToPage);
    });
  }

  _generateMarkup() {
    const numberOfPages = Math.ceil(
      this._data.results.length / this._data.resultsPerPage
    );
    console.log(numberOfPages);
    const currentPage = Number(this._data.page);

    // page 1 with other pages
    if (currentPage === 1 && numberOfPages > 1) {
      return this._generateNextPageBtn(currentPage);
    }

    // last page
    if (currentPage === numberOfPages && numberOfPages > 1) {
      return this._generatePrevPageBtn(currentPage);
    }

    // other pages
    if (currentPage < numberOfPages) {
      return `${this._generatePrevPageBtn(
        currentPage
      )}${this._generateNextPageBtn(currentPage)}`;
    }

    // page 1 without other pages
    return "";
  }

  _generatePrevPageBtn(page) {
    return `
    <button data-goto="${page - 1}" class="btn--inline pagination__btn--prev">
        <svg class="search__icon">
            <use href="${icons}#icon-arrow-left"></use>
        </svg>
        <span>Page ${page - 1}</span>
    </button>
    `;
  }

  _generateNextPageBtn(page) {
    return `
    <button data-goto="${page + 1}" class="btn--inline pagination__btn--next">
        <span>Page ${page + 1}</span>
        <svg class="search__icon">
            <use href="${icons}#icon-arrow-right"></use>
        </svg>
    </button>
    `;
  }
}

export default new PaginationView();
